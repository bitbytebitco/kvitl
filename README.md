# Description
This is the Android frontend for the "Kvitl" coffee bar tab system, which was provided 
to a client of Bit Byte Bit's to replace the legacy paper system. This frontend
provides a way for the business operator (and/or employees) to add customers, lookup customers,
add credits to a customer's account as well as debit purchases from their account balance. 

## Screenshots
Screenshots of this application can be seen [here](https://bitbytebit.co/project/rostovs)

## Releases
* v.1
* v.0.9.5
* v.0.9.4
* v.0.9.3
* v.0.9.2
* v.0.9.1
