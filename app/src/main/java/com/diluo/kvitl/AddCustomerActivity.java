package com.diluo.kvitl;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class AddCustomerActivity extends AppCompatActivity {

    private class AddCustomerTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            String result = apiRequest.addCustomer(params[0], params[1], params[2]);
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                //JSONArray custJson = new JSONArray(result);
                JSONObject jsonResult = new JSONObject(result);
                String resultProp = jsonResult.getString("result");
                Log.d("resultProp", resultProp);
                if(resultProp.equals("ok")){
                    //Intent intent = new Intent(AddCustomerActivity.this, MainActivity.class);
                    //startActivity(intent);

                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Log.d("resultProp", resultProp);
                }
//                JsonObject obj = new JsonParser().parse(jsonString).getAsJsonObject();

                //Log.d("add_cust", custJson.toString());
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.activity_add_customer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.ic_rostovs_r);
        setSupportActionBar(toolbar);

        Button cancelButton = (Button) findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    hideSoftKeyboard(AddCustomerActivity.this);
                } catch (Exception e) {
                    Log.d("hide_keyboard", e.getMessage());
                }

                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        Button clickButton = (Button) findViewById(R.id.submit_button);
        clickButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Log.d("CT","test this clicking");

                EditText customer_name = (EditText)findViewById(R.id.cust_name);
                String name = customer_name.getText().toString().trim();

                EditText cust_email = (EditText)findViewById(R.id.cust_email);
                String email = cust_email.getText().toString().trim();

                int newsletter_flag;
                CheckBox newsletterCheckbox = findViewById(R.id.newsletter_chkbox);
                if(newsletterCheckbox.isChecked()){
                    Log.d("checked_ckbx", "yes");
                    newsletter_flag = 1;
                } else {
                    Log.d("checked_ckbx", "no");
                    newsletter_flag = 0;
                }

                //String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());

                //KvitlDbHelper mDbHelper = new KvitlDbHelper(AddCustomerActivity.this);
                //mDbHelper.addCustomerRecord(fn, ln, timeStamp, email);
                AddCustomerTask addCustomerTask = new AddCustomerTask();
                addCustomerTask.execute(name, email, Integer.toString(newsletter_flag));

                //Intent intent = new Intent(AddCustomerActivity.this, MainActivity.class);
                //startActivity(intent);
            }
        });

    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

}

