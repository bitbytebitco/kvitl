package com.diluo.kvitl;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static com.diluo.kvitl.MainActivity.RELOAD_CUSTOMERS;

/**
 * Created by beckerz on 10/15/18.
 */

public class EditSettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.activity_edit_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.ic_rostovs_r);
        setSupportActionBar(toolbar);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String api_host = prefs.getString("api_host", "missing");
        String api_algorithm = prefs.getString("api_algorithm", "missing");
        String api_key = prefs.getString("api_key", "missing");
        String api_secret = prefs.getString("api_secret", "missing");

        EditText api_host_field = (EditText) findViewById(R.id.host);
        EditText api_algorithm_field = (EditText) findViewById(R.id.digest_algorithm);
        EditText api_key_field = (EditText) findViewById(R.id.digest_key);
        EditText api_secret_field = (EditText) findViewById(R.id.digest_secret);

        api_host_field.setText(api_host);
        api_algorithm_field.setText(api_algorithm);
        api_key_field.setText(api_key);
        api_secret_field.setText(api_secret);

        Button saveButton = (Button) findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText api_host_field = (EditText) findViewById(R.id.host);
                String api_host = api_host_field.getText().toString();

                EditText algorithm_field = (EditText) findViewById(R.id.digest_algorithm);
                String algorithm = algorithm_field.getText().toString();
                EditText digest_key_field = (EditText) findViewById(R.id.digest_key);
                String digest_key = digest_key_field.getText().toString();
                EditText digest_secret_field = (EditText) findViewById(R.id.digest_secret);
                String digest_secret = digest_secret_field.getText().toString();

                if(!api_host.isEmpty() && !algorithm.isEmpty() && !digest_key.isEmpty() && !digest_secret.isEmpty()){
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                    editor.putString("api_host", api_host);
                    editor.putString("api_algorithm", algorithm);
                    editor.putString("api_key", digest_key);
                    editor.putString("api_secret", digest_secret);
                    boolean write_test = editor.commit();
                    Intent intent = new Intent(EditSettingsActivity.this, MainActivity.class);
                    setResult(RELOAD_CUSTOMERS, intent);
                    finish();
                }
            }
        });

        Button cancelButton = findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Intent intent = new Intent(EditSettingsActivity.this, MainActivity.class);
            setResult(RESULT_OK, intent);
            finish();
            }
        });

    }


}
