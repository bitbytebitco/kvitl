package com.diluo.kvitl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by softchalk on 1/9/17.
 */
public class KvitlDbHelper extends SQLiteOpenHelper {

    private static final String SQL_CREATE_TABLE_CUSTOMERS =
            "CREATE TABLE " + CustomerReaderContract.CustomerEntry.TABLE_NAME + " (" +
                    CustomerReaderContract.CustomerEntry._ID + " INTEGER PRIMARY KEY," +
                    CustomerReaderContract.CustomerEntry.COLUMN_FIRST_NAME + " TEXT," +
                    CustomerReaderContract.CustomerEntry.COLUMN_LAST_NAME + " TEXT," +
                    CustomerReaderContract.CustomerEntry.COLUMN_DATE_ADDED + " TEXT," +
                    CustomerReaderContract.CustomerEntry.COLUMN_EMAIL + " TEXT," +
                    CustomerReaderContract.CustomerEntry.COLUMN_COFFEE_CLUB_MEMBER + " TEXT," +
                    CustomerReaderContract.CustomerEntry.COLUMN_TAB_BALANCE + " TEXT," +
                    CustomerReaderContract.CustomerEntry.COLUMN_ENABLED + " TEXT" +
            ")";

    private static final String SQL_CREATE_TABLE_TAB_NOTES =
            "CREATE TABLE " + TicketReaderContract.Ticket.TABLE_NAME + " (" +
                    TicketReaderContract.Ticket._ID + " INTEGER PRIMARY KEY," +
                    TicketReaderContract.Ticket.COLUMN_NAME_CUSTOMER_ID + " TEXT," +
                    TicketReaderContract.Ticket.COLUMN_NAME_NOTE_TYPE + " TEXT," +
                    TicketReaderContract.Ticket.COLUMN_NAME_NOTE_AMOUNT + " TEXT" +
                    ")";

    private static final String SQL_DELETE_CUSTOMER_ENTRIES =
            "DROP TABLE IF EXISTS " + CustomerReaderContract.CustomerEntry.TABLE_NAME;

    private static final String SQL_DELETE_TABNOTE_ENTRIES =
            "DROP TABLE IF EXISTS " + TicketReaderContract.Ticket.TABLE_NAME;

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 7;
    public static final String DATABASE_NAME = "Kvitl.db";

    public KvitlDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_CUSTOMERS);
        db.execSQL(SQL_CREATE_TABLE_TAB_NOTES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_CUSTOMER_ENTRIES);
        db.execSQL(SQL_DELETE_TABNOTE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    /**
     *
     * @param first_name
     * @param last_name
     * @param date_added
     * @return
     */
    public long addCustomerRecord(String first_name, String last_name, String date_added, String email) {
        // Gets the data repository in write mode
        SQLiteDatabase dbw = this.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(CustomerReaderContract.CustomerEntry.COLUMN_FIRST_NAME, first_name);
        values.put(CustomerReaderContract.CustomerEntry.COLUMN_LAST_NAME, last_name);
        values.put(CustomerReaderContract.CustomerEntry.COLUMN_DATE_ADDED, date_added);
        values.put(CustomerReaderContract.CustomerEntry.COLUMN_EMAIL, email);
        values.put(CustomerReaderContract.CustomerEntry.COLUMN_COFFEE_CLUB_MEMBER, "0");
        values.put(CustomerReaderContract.CustomerEntry.COLUMN_TAB_BALANCE, "0");
        values.put(CustomerReaderContract.CustomerEntry.COLUMN_ENABLED, "1");

        // Insert the new row, returning the primary key value of the new row
        long newRowId = dbw.insert(CustomerReaderContract.CustomerEntry.TABLE_NAME, null, values);
        return newRowId;
    }

    /**
     *
     * @param customer_id
     */
    public void deleteCustomerById(String customer_id) {
        SQLiteDatabase dbw = this.getWritableDatabase();

        String where = CustomerReaderContract.CustomerEntry._ID + " = " + customer_id;
        ContentValues cv = new ContentValues();
        cv.put(CustomerReaderContract.CustomerEntry.COLUMN_ENABLED, "0");
        dbw.update(CustomerReaderContract.CustomerEntry.TABLE_NAME, cv, where, null);
    }

    /**
     *
     * @param mDbHelper
     */
    public void deleteTestRecords(KvitlDbHelper mDbHelper) {
        //SQLiteDatabase dbw = mDbHelper.getWritableDatabase();

        // Define 'where' part of query.
        //String selection = FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE + " = ?";
        // Specify arguments in placeholder order.
        //String[] selectionArgs = { "title1" };
        // Issue SQL statement.
        //dbw.delete(FeedReaderContract.FeedEntry.TABLE_NAME, selection, selectionArgs);
    }

    /**
     *
     * @return Cursor
     */
    public Cursor getCustomerRecords() {
//        ArrayList records = new ArrayList<>();

        SQLiteDatabase dbr = this.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                CustomerReaderContract.CustomerEntry._ID,
                CustomerReaderContract.CustomerEntry.COLUMN_FIRST_NAME,
                CustomerReaderContract.CustomerEntry.COLUMN_LAST_NAME,
                CustomerReaderContract.CustomerEntry.COLUMN_DATE_ADDED,
                CustomerReaderContract.CustomerEntry.COLUMN_EMAIL,
                CustomerReaderContract.CustomerEntry.COLUMN_COFFEE_CLUB_MEMBER,
                CustomerReaderContract.CustomerEntry.COLUMN_TAB_BALANCE,
                CustomerReaderContract.CustomerEntry.COLUMN_ENABLED,
        };

        String sortOrder =
                CustomerReaderContract.CustomerEntry.COLUMN_LAST_NAME + " DESC";

        Cursor cursor = dbr.query(
                CustomerReaderContract.CustomerEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        return cursor;
    }

    /**
     *
     * @param queryString
     * @return Cursor records
     */
    public Cursor getCustomerRecordsWhereName( String queryString) {

        SQLiteDatabase dbr = this.getReadableDatabase();

        String[] projection = {
                CustomerReaderContract.CustomerEntry._ID,
                CustomerReaderContract.CustomerEntry.COLUMN_FIRST_NAME,
                CustomerReaderContract.CustomerEntry.COLUMN_LAST_NAME,
                CustomerReaderContract.CustomerEntry.COLUMN_DATE_ADDED,
                CustomerReaderContract.CustomerEntry.COLUMN_EMAIL,
                CustomerReaderContract.CustomerEntry.COLUMN_COFFEE_CLUB_MEMBER,
                CustomerReaderContract.CustomerEntry.COLUMN_TAB_BALANCE,
                CustomerReaderContract.CustomerEntry.COLUMN_ENABLED,
        };

        String selection = CustomerReaderContract.CustomerEntry.COLUMN_FIRST_NAME + " LIKE ? OR " + CustomerReaderContract.CustomerEntry.COLUMN_LAST_NAME + " LIKE ?" ;
        String[] selectionArgs = { "%" + queryString + "%", "%" + queryString + "%" };

        String sortOrder =
                CustomerReaderContract.CustomerEntry.COLUMN_FIRST_NAME + " DESC";

        Cursor cursor = dbr.query(
                CustomerReaderContract.CustomerEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        return cursor;
    }


    public Cursor getRecordsWhereId( int recordId) {

        SQLiteDatabase dbr = this.getReadableDatabase();

        String selection = CustomerReaderContract.CustomerEntry._ID + " = " + recordId;
        Log.d("DB", selection);

        Cursor cursor = dbr.rawQuery("select * from " + CustomerReaderContract.CustomerEntry.TABLE_NAME + " WHERE " + selection, null);
        return cursor;
    }

}
