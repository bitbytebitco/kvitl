package com.diluo.kvitl;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import cz.msebera.android.httpclient.Header;


import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    // Keep a reference to the NetworkFragment, which owns the AsyncTask object
    // that is used to execute network ops.
    //private NetworkFragment mNetworkFragment;

    // Boolean telling us whether a download is in progress, so we don't trigger overlapping
    // downloads with consecutive button clicks.
    //private boolean mDownloading = false;

//    public static ArrayList<String> customers = new ArrayList<String>();
    static final int RELOAD_CUSTOMERS = 113;
    protected JSONObject custJson;

    private class GetCustomersTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            String customers = apiRequest.getCustomers();
            return customers;
        }

        @Override
        protected void onPreExecute() {
            ConnectivityManager cm = (ConnectivityManager) MainActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                // If no connectivity, cancel task and update Callback with null data.
                Log.d("no_connectivity", "no_connectivity");
                //startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                networkErrorDialog();
                cancel(true);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                JSONArray custJson = new JSONArray(result);
                ArrayList customers = Customer.fromJson(custJson);
                CustomersAdapter adapter = new CustomersAdapter(MainActivity.this, customers);
                ListView myList = (ListView) findViewById(R.id.mobile_list);
                myList.setAdapter(adapter);
            } catch(Exception e) {
                searchErrorDialog();
                e.printStackTrace();
            }
        }
    }

    public void networkErrorDialog(){
        DialogFragment newFragment = new NetworkErrorDialogFragment();
        newFragment.show(this.getSupportFragmentManager(), "network_error");
    }

    public void searchErrorDialog() {
        SearchErrorDialogFragment searchErrorDialogFragment = new SearchErrorDialogFragment();
        searchErrorDialogFragment.show(this.getSupportFragmentManager(), "search_error");
    }

    private class GetCustomersByNameQueryTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            String queryString = params[0];
            String customers = apiRequest.getCustomerByNameQuery(queryString);
            return customers;
        }

        @Override
        protected void onPreExecute() {
            Connection conn = new Connection();
            if(!conn.isConnected(getApplicationContext())){
                networkErrorDialog();
                cancel(true);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONArray custJson = new JSONArray(result);
                ArrayList customers = Customer.fromJson(custJson);
                CustomersAdapter adapter = new CustomersAdapter(MainActivity.this, customers);
                ListView myList = (ListView) findViewById(R.id.mobile_list);
                myList.setAdapter(adapter);
            } catch(Exception e) {
                searchErrorDialog();
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.ic_rostovs_r);
        setSupportActionBar(toolbar);

//        try {
//            Log.d("api_host_prefs", "api_host_prefs attempt");
//            //SharedPreferences.Editor editor = getSharedPreferences("app_settings", MODE_PRIVATE).edit();
//            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
//            editor.putString("api_host", "https://diluo.org:300");
//            boolean test = editor.commit();
//            Log.d("api_host_prefs1", Boolean.toString(test));
//        } catch (Exception e) {
//            Log.d("api_host_error", e.toString());
//        }


        //mNetworkFragment = NetworkFragment.getInstance(getSupportFragmentManager(), "https://diluo.org:300/api/customers");

        // Add User Button
        FloatingActionButton fab_add_user = (FloatingActionButton) findViewById(R.id.fab_add_user);
        fab_add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent intent = new Intent(MainActivity.this, AddCustomerActivity.class);
                startActivityForResult(intent, 0);
            }
        });

        ListView myList = (ListView) findViewById(R.id.mobile_list);
        myList.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                // TODO: replace with API drive json focused approach
//                SQLiteCursor item = (SQLiteCursor) parent.getAdapter().getItem(position);
//                Long record_id = item.getLong(item.getColumnIndexOrThrow("_id"));
//                Log.d("CLICKS", "You selected : " + record_id);
                Customer item = (Customer) parent.getItemAtPosition(position);

                Log.d("ITEM", item.getName());
                Log.d("ITEM", item.getEmail());

                String customer_id = Integer.toString(item.getId());
                Log.d("ITEM", customer_id);

                Intent intent = new Intent(getApplicationContext(), ViewCustomerActivity.class);
                intent.putExtra("CUSTOMER_ID", customer_id);
                startActivity(intent);
            }
        });


        GetCustomersTask customersTask = new GetCustomersTask();
        customersTask.execute();
        Button clickButton = (Button) findViewById(R.id.cust_search_submit);
        clickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("CT","test this clicking2222222");

                EditText cust_search = (EditText)findViewById(R.id.cust_search);
                String queryString = cust_search.getText().toString();

                if (queryString.isEmpty()){
                    GetCustomersTask customersTask = new GetCustomersTask();
                    customersTask.execute();
                } else {
                    GetCustomersByNameQueryTask customerQueryTask = new GetCustomersByNameQueryTask();
                    customerQueryTask.execute(queryString);
                }

                hideSoftKeyboard(MainActivity.this);

//                KvitlDbHelper mDbHelper = new KvitlDbHelper(MainActivity.this);
//
//                Cursor recordsCursor = mDbHelper.getCustomerRecordsWhereName(queryString);
//                ListView myList = (ListView) findViewById(R.id.mobile_list);
//                CustomerCursorAdapter myAdapter = new CustomerCursorAdapter(MainActivity.this, recordsCursor);
//                myList.setAdapter(myAdapter);

            }
        });



    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            hideSoftKeyboard(MainActivity.this);
        } catch (Exception e) {
            Log.d("updateFromDownload", e.getMessage());
        }

        GetCustomersTask customersTask = new GetCustomersTask();
        customersTask.execute();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("menu item: ", Integer.toString(item.getItemId()));
        switch(item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_edit_settings:
                Log.d("clicked_a_thing", "you clicked a thingy");
                Intent intent = new Intent(getApplicationContext(), EditSettingsActivity.class);
                startActivityForResult(intent, 0);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.customers_main_menu, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        Log.d("onActivityResult", Integer.toString(requestCode));
        if (requestCode == RELOAD_CUSTOMERS) {
            GetCustomersTask customersTask = new GetCustomersTask();
            customersTask.execute();
        }
    }

}
