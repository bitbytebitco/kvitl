package com.diluo.kvitl;


import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

/**
 * Created by beckerz on 10/9/17.
 */

public class ProductOption implements Parcelable {
    public int id;
    public int parent_id;
    public int priority;
    public String name;
    public BigDecimal price;

    public ProductOption(int id, int parent_id, int priority, String name, BigDecimal price) {
        this.id = id;
        this.parent_id = parent_id;
        this.priority = priority;
        this.name = name;
        this.price = price;
    }

    public ProductOption(ProductOption po) {
        this.id = po.id;
        this.parent_id = po.parent_id;
        this.priority = po.priority;
        this.name = po.name;
        this.price = po.price;
    }

    public int getId() {
        return this.id;
    }

    public int getParent_id() {
        return this.parent_id;
    }

//    public Product getProduct() {
//
//        return p;
//    }

    public int getPriority() {
        return this.priority;
    }

    public String getName() {
        return this.name;
    }

    public String getPrice() {
        return this.price.toString();
    }

    public static ProductOption fromJson(JSONObject jsonData) {
        ProductOption po = null;
        try {
            int id = jsonData.getInt("prod_opt_id");
            int parent_id = jsonData.getInt("prod_opt_parent_id");

            int priority = jsonData.getInt("prod_opt_priority");
            String name = jsonData.getString("prod_opt_name");
            BigDecimal price = new BigDecimal(jsonData.getString("prod_opt_price"));
            po = new ProductOption(id, parent_id, priority, name, price);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return po;
    }

    public static ArrayList fromJson(JSONArray jsonArray) {
        JSONObject optJson;
        ArrayList<ProductOption> options = new ArrayList<ProductOption>(jsonArray.length());
        for (int i=0; i < jsonArray.length(); i++) {
            try {
                optJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
            ProductOption prodOpt = ProductOption.fromJson(optJson);
            if(prodOpt != null) {
                options.add(prodOpt);
            }
        }
        return options;
    }

    protected BigDecimal bigDecimalFromString(String strNum) {
        // Create a DecimalFormat that fits your requirements
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        symbols.setDecimalSeparator('.');
        String pattern = "#,##0.0#";
        DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
        decimalFormat.setParseBigDecimal(true);

        try {
            // parse the string
            BigDecimal bigDecimal = (BigDecimal) decimalFormat.parse(strNum);
            return bigDecimal;
        } catch (Exception e) {
            return null;
        }
    }

    public ProductOption(Parcel in) {
        String[] data = new String[5];
        in.readStringArray(data);
        this.id = Integer.parseInt(data[0]);
        this.parent_id = Integer.parseInt(data[1]);
        this.priority = Integer.parseInt(data[2]);
        this.name = data[3];
        this.price = bigDecimalFromString(data[4]);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {Integer.toString(this.id), Integer.toString(this.parent_id), Integer.toString(this.priority), this.name, this.price.toString()});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ProductOption createFromParcel(Parcel in) {
            return new ProductOption(in);
        }

        public ProductOption[] newArray(int size) {
            return new ProductOption[size];
        }
    };
}
