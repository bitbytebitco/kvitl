package com.diluo.kvitl;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONObject;

public class EditCustomerActivity extends AppCompatActivity implements DeleteCustomerDialogFragment.DialogListener {

    private class EditCustomerTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            String result = apiRequest.EditCustomer(params[0], params[1], params[2]);
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                //JSONArray custJson = new JSONArray(result);
                JSONObject jsonResult = new JSONObject(result);
                String resultProp = jsonResult.getString("result");
                Log.d("resultProp", resultProp);
                if(resultProp.equals("ok")){
                    Intent intent = new Intent();
                    String customer_id = getIntent().getStringExtra("CUSTOMER_ID");
                    if(customer_id != null) {
                        Log.d("customer_id", customer_id);
                        intent.putExtra("CUSTOMER_ID", customer_id);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                } else {
                    Log.d("resultProp", resultProp);
                }

                //Log.d("add_cust", custJson.toString());
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void startMainActivity() {
        Intent intent = new Intent(EditCustomerActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public void deleteCustomerDialog(){
        DialogFragment newFragment = new DeleteCustomerDialogFragment();
        newFragment.show(this.getSupportFragmentManager(), "delete_customer");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_customer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.ic_rostovs_r);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(findViewById(R.id.view_edit_cust_fragment) != null){
            if (savedInstanceState != null) {
                return;
            }

            final String customer_id = getIntent().getStringExtra("CUSTOMER_ID");
            Log.d("edit_cust_act_cst_id", customer_id);
            ViewEditCustFragment editCustFragment = ViewEditCustFragment.newInstance(customer_id);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.view_edit_cust_fragment, editCustFragment).commit();

//            EditText emailInput = findViewById(R.id.cust_email);
//            emailInput.setEnabled(false);
//            emailInput.setFocusable(false);

            Button saveButton = findViewById(R.id.save_edit_button);
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EditText cust_name = findViewById(R.id.cust_name);
                    //CheckBox coffee_club = findViewById(R.id.coffee_chkbox);
                    CheckBox newsletter = findViewById(R.id.newsletter_chkbox);

                    Log.d("on_save_click", "on_save_click");
                    String customer_name = cust_name.getText().toString();
                    Log.d("cust_name", customer_name);

//                    String coffee_club_flag;
//                    if(coffee_club.isChecked()){
//                        Log.d("coffee_club", "checked");
//                        coffee_club_flag = "1";
//                    } else {
//                        Log.d("coffee_club", "unchecked");
//                        coffee_club_flag = "0";
//                    }

                    String newsletter_flag;
                    if(newsletter.isChecked()){
                        Log.d("newsletter", "checked");
                        newsletter_flag = "1";
                    } else {
                        Log.d("newsletter", "unchecked");
                        newsletter_flag = "0";
                    }

                    String customer_id = getIntent().getStringExtra("CUSTOMER_ID");

                    EditCustomerTask editCustomerTask = new EditCustomerTask();
                    editCustomerTask.execute(customer_id, customer_name, newsletter_flag);
                }
            });

            Button cancelButton = findViewById(R.id.cancel_edit_button);
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    String customer_id = getIntent().getStringExtra("CUSTOMER_ID");
                    if(customer_id != null) {
                        Log.d("customer_id", customer_id);
                        intent.putExtra("CUSTOMER_ID", customer_id);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            });

        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("menu item: ", Integer.toString(item.getItemId()));
        switch(item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_delete_customer:
                deleteCustomerDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_customer_menu, menu);
        return true;
    }

}
