package com.diluo.kvitl;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by beckerz on 10/17/17.
 */

public class AddCreditDialogFragment extends DialogFragment {
    static AddCreditDialogFragment newInstance(String customer_id){
        AddCreditDialogFragment f = new AddCreditDialogFragment();
        Bundle args = new Bundle();
        args.putString("customer_id", customer_id);
        f.setArguments(args);
        return f;
    }

    public class Wrapper
    {
        public String result;
        public String customer_id;
    }

    public interface DialogListener {
        void triggerTransactionListRefresh();
        void refreshBalance();
    }
    DialogListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    private class CreditAccountTask extends AsyncTask<String, Integer, Wrapper> {
        @Override
        protected Wrapper doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            String result = apiRequest.creditAccount(params[0], params[1]);
            Wrapper w = new Wrapper();
            w.result = result;
            w.customer_id = params[0];
            return w;
        }

        @Override
        protected void onPostExecute(Wrapper w) {
            try {
                Log.d("onPostExecute", w.result);
                //JSONArray custJson = new JSONArray(result);
                JSONObject jsonResult = new JSONObject(w.result);
                String resultProp = jsonResult.getString("result");
                Log.d("resultProp", resultProp);
                if(resultProp.equals("ok")){
//                    Intent intent = new Intent(getActivity(), ViewCustomerActivity.class);
//                    intent.putExtra("CUSTOMER_ID", w.customer_id);
//                    startActivity(intent);
                } else {
                    Log.d("resultProp", resultProp);
                }
//                JsonObject obj = new JsonParser().parse(jsonString).getAsJsonObject();

                //Log.d("add_cust", custJson.toString());
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        EditText mEditText = (EditText) view.findViewById(R.id.add_credit_amount);
        // Show soft keyboard automatically and request focus to field
        mEditText.requestFocus();
//        final InputMethodManager inputMethodManager =
//                (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//
//        inputMethodManager.showSoftInput(mEditText, InputMethodManager.SHOW_FORCED);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflator = getActivity().getLayoutInflater();
        final View view = inflator.inflate(R.layout.dialog_add_credit, null);

        Bundle bundle = getArguments();
        final String customer_id = bundle.getString("customer_id");

        builder.setMessage(R.string.dialog_add_credit)
                .setPositiveButton(R.string.add_credit_submit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
//                        String record_id = getActivity().getIntent().getStringExtra("RECORD_ID");
//                        Log.d("DIALOG", "Delete customer " + record_id);
//                        KvitlDbHelper mDbHelper = new KvitlDbHelper(getActivity());
//                        mDbHelper.deleteCustomerById(record_id);
                        EditText amountVal = (EditText) view.findViewById(R.id.add_credit_amount);
                        Log.d("cust_id_2", customer_id);
                        CreditAccountTask creditAccountTask = new CreditAccountTask();
                        creditAccountTask.execute(customer_id, amountVal.getText().toString());

//                        Intent intent = new Intent(view.getContext(), ViewCustomerActivity.class);
//                        intent.putExtra("CUSTOMER_ID", customer_id);
//                        startActivity(intent);
                        //ViewCustomerActivity appCont = (ViewCustomerActivity) view.getContext();

                        mListener.refreshBalance();
                        mListener.triggerTransactionListRefresh();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dismiss();
                    }
                });
        builder.setView(view);

        // Create the AlertDialog object and return it
        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                try {
                    EditText mEditText = (EditText) view.findViewById(R.id.add_credit_amount);
                    mEditText.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(mEditText, InputMethodManager.SHOW_IMPLICIT);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        return alertDialog;
    }
}
