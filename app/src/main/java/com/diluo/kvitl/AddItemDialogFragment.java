package com.diluo.kvitl;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by beckerz on 10/7/17.
 */

public class AddItemDialogFragment extends DialogFragment {

    ExpandableListView expListView;
    Button cancelButton;
    Button confirmButton;

    static AddItemDialogFragment newInstance(ArrayList products) {
        AddItemDialogFragment f = new AddItemDialogFragment();

        Bundle args = new Bundle();
//        adf.setArguments(args);
        args.putParcelableArrayList("PARCEL_PRODUCTS", products);
        f.setArguments(args);

        return f;
    }

    public interface DialogListener {
        void populateSelectedItems(ArrayList<Product> selected);
    }
    DialogListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    // Convert pixel to dip
    public int getDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        //AlertDialog.Builder builder;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
//        } else {
//            builder = new AlertDialog.Builder(getActivity());
//        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflator = getActivity().getLayoutInflater();
        View view = inflator.inflate(R.layout.dialog_add_item, null);

        Bundle bundle = getArguments();
        ArrayList products = (ArrayList) bundle.getParcelableArrayList("PARCEL_PRODUCTS");

        expListView = (ExpandableListView) view.findViewById(R.id.product_list);
        final ExpandableListAdapter expListAdapter = new ExpandableListAdapter(getActivity(), products);
        //expListAdapter.checkedProducts = new ArrayList<>();
        expListAdapter.clearCheckedProducts();
        expListView.setAdapter(expListAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Log.d("onChildClick","onChildClick");
                Log.d("case_test","case_test");

                final String selected = expListAdapter.getChild(groupPosition, childPosition).getName();
                Toast.makeText(getActivity(), selected, Toast.LENGTH_LONG).show();

                return true;
            }
        });

        //builder.setMessage(R.string.dialog_add_item);
//        builder.setPositiveButton(R.string.add_item_submit, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                ArrayList<Product> selected = expListAdapter.getCheckedProducts();
//                for(Product product : selected) {
//                    Log.d("checked", product.getName() + " - " + product.getSelected_option().getName());
//                }
//                mListener.populateSelectedItems(selected);
//            }
//        });
        confirmButton = (Button) view.findViewById(R.id.confirm_add_items);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Product> selected = expListAdapter.getCheckedProducts();
                for(Product product : selected) {
                    Log.d("checked", product.getName() + " - " + product.getSelected_option().getName());
                }
                mListener.populateSelectedItems(selected);
                dismiss();
            }
        });

        cancelButton = (Button) view.findViewById(R.id.cancel_add_items);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

//        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                // User cancelled the dialog
//                dismiss();
//            }
//        });

        builder.setView(view);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;

        lp.copyFrom(alertDialog.getWindow().getAttributes());
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        lp.width = dialogWindowWidth;
        int dialogWindowHeight = (int) (displayHeight * 0.9f);
        lp.height = dialogWindowHeight;
        alertDialog.getWindow().setAttributes(lp);

        // Create the AlertDialog object and return it
        return alertDialog;
    }

}
