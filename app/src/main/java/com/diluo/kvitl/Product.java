package com.diluo.kvitl;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by beckerz on 10/7/17.
 */

public class Product implements Parcelable {
    public int id;
    public String name;
    public String type;
    public ArrayList<ProductOption> options;
    public ProductOption selected_option;

    public Product(int id, String name, String type, ArrayList<ProductOption> options) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.options = options;
    }

    public Product(Product p) {
        this.id = p.id;
        this.name = p.name;
        this.type = p.type;
        this.options = p.options;
        this.selected_option = p.selected_option;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public ArrayList getOptionList() {
        return this.options;
    }

    public void setSelectedOption(ProductOption productOption) {
        this.selected_option = productOption;
    }

    public ProductOption getSelected_option() {
        return this.selected_option;
    }

    public static Product fromJson(JSONObject jsonProd) {
        Product p = null;

        try {
            int id = jsonProd.getInt("prod_id");
            String name = jsonProd.getString("prod_name");
            String type = jsonProd.getString("prod_type");

            JSONArray jsonOptions = jsonProd.getJSONArray("options");

            ArrayList<ProductOption> options = ProductOption.fromJson(jsonOptions);
            p = new Product(id, name, type, options);

        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
        return p;
    }

    public static ArrayList fromJson(JSONArray jsonArray) {
        JSONObject productJson;
        ArrayList product_ids = new ArrayList();
        ArrayList<Product> products = new ArrayList<Product>(jsonArray.length());
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    productJson = jsonArray.getJSONObject(i);
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
                Product product = Product.fromJson(productJson);
                if (product != null && !product_ids.contains(product.getId())) {
                    products.add(product);
                    product_ids.add(product.getId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return products;
    }

    // Parcelling
    public Product(Parcel in) {
        String[] data = new String[3];
        in.readStringArray(data);
        this.id = Integer.parseInt(data[0]);
        this.name = data[1];
        this.type = data[2];
        this.options = in.readArrayList(null);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {Integer.toString(this.id), this.name, this.type});
        dest.writeList(this.options);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
