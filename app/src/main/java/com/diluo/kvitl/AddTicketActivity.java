package com.diluo.kvitl;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import static com.diluo.kvitl.ViewCustomerActivity.ADD_TICKET_REQUEST;

/**
 * Created by softchalk on 2/16/17.
 */

public class AddTicketActivity extends AppCompatActivity implements AddItemDialogFragment.DialogListener {

    static ArrayList<Product> products_w_options = new ArrayList<Product>();
    static ArrayList<String> products = new ArrayList<String>();
    static ArrayList<Product> selected_products = new ArrayList<Product>();

    private class GetProductsTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            String products = apiRequest.getProducts();
            return products;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                JSONArray prodJson = new JSONArray(result);
                Log.d("getProducts", prodJson.toString());
                products = Product.fromJson(prodJson);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetProductsWOptionsTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            String products = apiRequest.getProductsWithOptions();
            return products;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                JSONArray prodJson = new JSONArray(result);
                Log.d("getProductsWithOptions", prodJson.toString());
                products_w_options = Product.fromJson(prodJson);
                addItemDialog(products_w_options);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetCustomerTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            int customer_id = Integer.parseInt(params[0]);
            String customer = apiRequest.getCustomerById(customer_id);
            return customer;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                JSONObject custJson = new JSONObject(result);
                Customer customer = Customer.fromJson(custJson);

                TextView customerName = (TextView) findViewById(R.id.ticket_customer_name);
                customerName.setText(customer.getName());

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class AddTransactionTask extends AsyncTask<Object, Integer, String> {
        @Override
        protected String doInBackground(Object... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            String customer_id = (String) params[0];
            String total = (String) params[1];
            String[] product_option_ids = (String[]) params[2];

            String addTransactionResponse = apiRequest.addTransaction(customer_id, total, product_option_ids);
            return addTransactionResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                JSONObject resultJson = new JSONObject(result);
                if(resultJson.getString("result").equals("ok")) {
                    Log.d("newIntent", "newIntent");
                    //Intent intent = new Intent(getApplicationContext(), ViewCustomerActivity.class);
                    Intent intent = new Intent();
                    String customer_id = getIntent().getStringExtra("CUSTOMER_ID");
                    if(customer_id != null) {
                        Log.d("customer_id", customer_id);
                        intent.putExtra("CUSTOMER_ID", customer_id);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

//    class TransactionHolder {
//        String customer_id;
//        String total;
//        String[] product_option_ids;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.activity_add_ticket);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.ic_rostovs_r);
        setSupportActionBar(toolbar);

        GetProductsWOptionsTask productsWOptionsTask = new GetProductsWOptionsTask();
        productsWOptionsTask.execute();

        final String customer_id = intent.getStringExtra("CUSTOMER_ID");
        if(customer_id != null) {
            GetCustomerTask customerTask = new GetCustomerTask();
            customerTask.execute(customer_id);
        } else {
            Log.d("customer_id","Customer_id is missing");
        }

        selected_products = new ArrayList<Product>();

        Button submitButton = (Button) findViewById(R.id.submit_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setEnabled(false);
                AddTransactionTask transactionTask = new AddTransactionTask();
                View ticket_amount = findViewById(R.id.ticket_amount);

                ListView selected = (ListView) findViewById(R.id.selected_items);
                SelectedTicketItemAdapter adapter = (SelectedTicketItemAdapter) selected.getAdapter();
                if(adapter != null) {
                    String[] product_option_ids = getStringArray(adapter);

                    if (product_option_ids.length > 0) {
                        transactionTask.execute(customer_id, adapter.getTotal(), product_option_ids);
                    }
                }
            }
        });

        Button cancelButton = (Button) findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected_products = new ArrayList<Product>();

                Intent intent = new Intent();
                String customer_id = getIntent().getStringExtra("CUSTOMER_ID");
                if(customer_id != null) {
                    Log.d("customer_id", customer_id);
                    intent.putExtra("CUSTOMER_ID", customer_id);
                    setResult(RESULT_OK, intent);
                    finish();
                }

                //Intent intent = new Intent(AddTicketActivity.this, MainActivity.class);
                //startActivity(intent);
            }
        });


//        Spinner spinner = (Spinner) findViewById(R.id.ticket_type_spinner);
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.ticket_types_array, android.R.layout.simple_spinner_dropdown_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(adapter);
    }

    public static String[] getStringArray(ListAdapter adapter){
        String[] a = new String[adapter.getCount()];

        for(int i=0; i<a.length; i++) {
            Product p = (Product) adapter.getItem(i);
            a[i] = Integer.toString(p.getSelected_option().getId());
        }
        return a;
    }

    public void populateSelectedItems(ArrayList<Product> new_products) {
        Log.d("populateSelectedItems", "populateSelectedItems");
        for(Product p:new_products){
            Log.d("new_prod", p.getSelected_option().getName());
            selected_products.add(p);
        }
        //selected_products.addAll(new_products);
        final SelectedTicketItemAdapter selectedListAdapter = new SelectedTicketItemAdapter(AddTicketActivity.this, selected_products);
        ListView selectedList = (ListView) findViewById(R.id.selected_items);
        selectedListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                TextView ticket_amount = (TextView) findViewById(R.id.ticket_amount);
                ticket_amount.setText(selectedListAdapter.getTotal());
            }
        });
        selectedList.setAdapter(selectedListAdapter);

        TextView ticket_amount = (TextView) findViewById(R.id.ticket_amount);
        ticket_amount.setText(selectedListAdapter.getTotal());
    }

    public void addItemDialog(ArrayList products) {
        DialogFragment newFragment = AddItemDialogFragment.newInstance(products);
        newFragment.show(this.getSupportFragmentManager(), "add_item");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.view_customer_add_item, menu);

        Drawable icon = DrawableCompat.wrap(AppCompatResources.getDrawable(AddTicketActivity.this, R.drawable.ic_add_shopping_cart_black_24dp));
        DrawableCompat.setTint(icon, Color.WHITE);
        menu.findItem(R.id.action_add_item).setIcon(icon);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("menu item: ", Integer.toString(item.getItemId()));
        switch(item.getItemId()){
            case R.id.action_add_item:
                addItemDialog(products_w_options);
                Log.d("test ", "test");
                return true;
        }
        return true;
    }

    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        Log.d("onSaveInstanceState", "onSaveInstanceState");

        try {
            outState.putParcelableArrayList("selected_products", selected_products);
        } catch(Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d("onRestoreInstanceState", "onRestoreInstanceState");

        try {
            selected_products = savedInstanceState.getParcelableArrayList("selected_products");

            //populateSelectedItems(selected_products);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
