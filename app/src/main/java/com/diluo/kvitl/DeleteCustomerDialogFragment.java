package com.diluo.kvitl;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Bit Byte Bit on 2/28/17.
 */

public class DeleteCustomerDialogFragment extends DialogFragment {

    public interface DialogListener {
        void startMainActivity();
    }
    DialogListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    private class MarkCustomerDeletedTask extends AsyncTask<Object, Integer, String> {
        @Override
        protected String doInBackground(Object... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            String customer_id = (String) params[0];

            String addTransactionResponse = apiRequest.markCustomerDeleted(customer_id);
            return addTransactionResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                JSONObject resultJson = new JSONObject(result);
                if(resultJson.getString("result").equals("ok")) {
                    Log.d("delete_worked", "delete_worked");
                    Log.d("newIntent", "newIntent");

                    mListener.startMainActivity();

                    //Intent intent = new Intent(context, MainActivity.class);
                    //startActivity(intent);

                    //Intent intent = new Intent(getApplicationContext(), ViewCustomerActivity.class);
//                    Intent intent = new Intent();
//                    String customer_id = getIntent().getStringExtra("CUSTOMER_ID");
//                    if(customer_id != null) {
//                        Log.d("customer_id", customer_id);
//                        intent.putExtra("CUSTOMER_ID", customer_id);
//                        setResult(RESULT_OK, intent);
//                        finish();
//                    }

                }

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_delete_customer)
                .setPositiveButton(R.string.confirm_delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!

                        Log.d("DIALOG", "attempt delete ");
                        try {
                            Log.d("DIALOG", "attempt delete ");
                            String customer_id = getActivity().getIntent().getStringExtra("CUSTOMER_ID");
                            Log.d("DIALOG", "Delete customer " + customer_id);
                            MarkCustomerDeletedTask getTransactionsTask = new MarkCustomerDeletedTask();
                            getTransactionsTask.execute(customer_id);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dismiss();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
