package com.diluo.kvitl;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by beckerz on 10/4/17.
 */

public class Customer {
    public int id;
    public String name;
    public String email;
    public boolean coffee_club_flag;
    public boolean newsletter_flag;

    public BigDecimal total;

    public int getId() {
        return this.id;
    }

    public String getName(){
        return this.name;
    }

    public String getEmail(){
        return this.email;
    }

    public boolean getCoffeeClubFlag() { return this.coffee_club_flag ; }

    public boolean getNewsletterFlag() { return this.newsletter_flag; }

    public String getTotal(){
        return this.total.toString();
    }

    public static Comparator<Customer> getCompByName(){
        Comparator<Customer> comp = new Comparator<Customer>() {
            @Override
            public int compare(Customer c1, Customer c2) {
                return c1.name.toLowerCase().compareTo(c2.name.toLowerCase());
            }
        };
        return comp;
    }

    public static Customer fromJson(JSONObject jsonCust) {
        Customer c = new Customer();
        try {
            Log.d("jsonCust", jsonCust.toString());
            c.id = jsonCust.getInt("id");
            c.name = jsonCust.getString("cust_name");
            c.email = jsonCust.getString("cust_email");
            c.total = new BigDecimal(jsonCust.getString("total"));
            if(jsonCust.has("newsletter_opt")) {
                boolean newsletter_flag = (jsonCust.getInt("newsletter_opt") > 0) ? true : false;
                c.newsletter_flag = newsletter_flag;
            }
            if(jsonCust.has("coffee_club")) {
                boolean coffee_club_flag = (jsonCust.getInt("coffee_club") > 0) ? true : false;
                c.coffee_club_flag = coffee_club_flag;
            }
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
        return c;
    }

    public static ArrayList fromJson(JSONArray jsonArray) {
        JSONObject customerJson;
        ArrayList<Customer> customers = new ArrayList<Customer>(jsonArray.length());
        for(int i=0; i < jsonArray.length(); i++){
            try {
                customerJson = jsonArray.getJSONObject(i);
            } catch(Exception e) {
                e.printStackTrace();
                continue;
            }
            Customer customer = Customer.fromJson(customerJson);
            if(customer != null){
                customers.add(customer);
            }
        }
        try {
            Collections.sort(customers, Customer.getCompByName());
        } catch(Exception e) {
            Log.d("sort_customers_error", e.getMessage());
        }
        return customers;
    }

}
