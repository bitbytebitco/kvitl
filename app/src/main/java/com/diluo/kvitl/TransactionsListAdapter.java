package com.diluo.kvitl;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by beckerz on 11/15/17.
 */



public class TransactionsListAdapter extends ArrayAdapter<Transaction> {

    ArrayList<Transaction> transactions;

    public TransactionsListAdapter(Context context, ArrayList<Transaction> transactions) {
        super(context, 0, transactions);
        this.transactions = transactions;
    }

    private class ViewHolder {
        TextView trans_date;
        TextView trans_type;
        TextView trans_total;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Transaction transaction = getItem(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.transaction_item_row, parent, false);
            viewHolder.trans_date = (TextView) convertView.findViewById(R.id.trans_date);
            viewHolder.trans_type = (TextView) convertView.findViewById(R.id.trans_type);
            viewHolder.trans_total = (TextView) convertView.findViewById(R.id.trans_total);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.trans_date.setText(transaction.getTrans_date());
        viewHolder.trans_type.setText(transaction.getTrans_type());
        viewHolder.trans_total.setText(transaction.getTrans_total());

        if(viewHolder.trans_type.getText().toString().equals("debit")) {
            viewHolder.trans_total.setTextColor(Color.parseColor("#ff0900"));
        } else {
            viewHolder.trans_total.setTextColor(Color.parseColor("#00664F"));
        }
        return convertView;
    }
}
