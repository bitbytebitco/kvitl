package com.diluo.kvitl;

import android.provider.BaseColumns;

import java.math.BigDecimal;

/**
 * Created by softchalk on 1/9/17.
 */

public final class CustomerReaderContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private CustomerReaderContract() {}

    /* Inner class that defines the table contents */
    public static class CustomerEntry implements BaseColumns {
        public static final String TABLE_NAME = "customers";
        public static final String COLUMN_FIRST_NAME = "customer_first_name";
        public static final String COLUMN_LAST_NAME = "customer_first_last";
        public static final String COLUMN_DATE_ADDED = "customer_date_added";
        public static final String COLUMN_EMAIL = "customer_email";
        public static final String COLUMN_COFFEE_CLUB_MEMBER = "customer_coffee_club_member";
        public static final String COLUMN_TAB_BALANCE = "customer_tab_balance";
        public static final String COLUMN_ENABLED = "customer_enabled";

    }
}

