package com.diluo.kvitl;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.diluo.kvitl.Customer;


/**
 * Created by Bit Byte Bit on 10/1/17.
 */

public class CustomersAdapter extends ArrayAdapter<Customer> {

    private static class ViewHolder {
        TextView name;
        TextView email;
    }

    public CustomersAdapter(Context context, ArrayList<Customer> customers){
        super(context, 0, customers);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Customer customer = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_layout, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.customerName);
            viewHolder.email = (TextView) convertView.findViewById(R.id.customerEmail);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        viewHolder.name.setText(customer.getName());
        viewHolder.email.setText(customer.getEmail());

        return convertView;

    }
}
