package com.diluo.kvitl;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by beckerz on 10/7/17.
 */

public class ProductsAdapter extends ArrayAdapter<Product> {

    private static class ViewHolder {
        TextView name;
        ListView options;
    }

    public ProductsAdapter(Context context, ArrayList<Product> products){
        super(context, 0, products);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Product product = getItem(position);
        ViewHolder viewHolder;

        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.product_row_layout, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.productName);
            viewHolder.options = (ListView) convertView.findViewById(R.id.productOptions);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.name.setText(product.getName());
        // TODO: add building of ListView options
        return convertView;
    }
}
