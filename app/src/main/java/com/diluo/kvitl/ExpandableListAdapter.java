package com.diluo.kvitl;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.security.AccessController.getContext;

/**
 * Created by beckerz on 10/11/17.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Activity context;
    private ArrayList<Product> products;
    public ArrayList<Product> checkedProducts = new ArrayList<>();
    private final Set<Pair<Long, Long>> mCheckedItems = new HashSet<Pair<Long, Long>>();
    int primaryColor;

    public ExpandableListAdapter(Activity context, ArrayList<Product> products) {
        this.context = context;
        this.products = products;
    }

    public ProductOption getChild(int groupPosition, int childPosition) {
        return (ProductOption) this.products.get(groupPosition).getOptionList().get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public Set<Pair<Long, Long>> getCheckedItems() {
        return mCheckedItems;
    }

    public void clearCheckedProducts() {
        try {
            checkedProducts = new ArrayList<>();
            Log.d("checkedPrds", checkedProducts.toString());
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Product> getCheckedProducts() {
        return checkedProducts;
    }

    private static class ChildViewHolder {
        TextView option_name;
        TextView option_price;
        //CheckBox option_checkbox;
        //Button add_item;
    }

    public boolean isChildChecked(Pair pair){
        if(mCheckedItems.contains(pair)){
            return true;
        }
        return false;
    }

    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder viewHolder;
        RelativeLayout child;

        final ProductOption product_option = getChild(groupPosition, childPosition);
        LayoutInflater inflater = context.getLayoutInflater();

        final Pair<Long, Long> tag = new Pair<Long, Long>(
                Long.valueOf(product_option.getParent_id()),
                Long.valueOf(product_option.getId())
        );

        //Log.d("mCheckedItems", tag.first.toString() + "," + tag.second.toString());
//        for (Pair cItem:mCheckedItems) {
            //Log.d("cItem", cItem.first.toString() + "," + cItem.second.toString());
//        }

        primaryColor = ContextCompat.getColor(context, R.color.colorPrimary);


        if (convertView == null) {
            convertView = inflater.inflate(R.layout.child_item, null);
            viewHolder = new ChildViewHolder();
            viewHolder.option_name = (TextView) convertView.findViewById(R.id.option_name);
            viewHolder.option_price = (TextView) convertView.findViewById(R.id.option_price);
            //viewHolder.option_checkbox = (CheckBox) convertView.findViewById(R.id.option_checkbox);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ChildViewHolder) convertView.getTag();
        }

        child = (RelativeLayout) convertView;
        child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RelativeLayout rl = (RelativeLayout) view;

                TextView option_name = (TextView) view.findViewById(R.id.option_name);
                TextView option_price = (TextView) view.findViewById(R.id.option_price);

                Product p = getGroup(groupPosition);
                Product pn = new Product(p);
                ProductOption po = new ProductOption(product_option);
                pn.setSelectedOption(po);

                if (!isChildChecked(tag)) {
                    Log.d("adding", "adding");
                    //Log.d("adding", Integer.toString(po.getId()));

                    rl.setBackgroundResource(R.color.colorPrimary);
                    option_name.setTextColor(Color.WHITE);
                    option_price.setTextColor(Color.WHITE);

                    mCheckedItems.add(tag);
                    checkedProducts.add(pn);
                } else {
                    Log.d("removing", "removing");
                    rl.setBackgroundColor(Color.WHITE);
                    option_name.setTextColor(primaryColor);
                    option_price.setTextColor(primaryColor);

                    for(Iterator<Product> it = checkedProducts.iterator(); it.hasNext();) {
                        Product prod = it.next();
                        Log.d("option_id", Integer.toString(prod.getSelected_option().getId()));
                        if(prod.getSelected_option().getId() == po.getId()) {
                            //checkedProducts.remove(prod);
                            it.remove();
                        }
                    }
                    mCheckedItems.remove(tag);
                }
            }
        });

        /** Checkbox stuff **/
        //            viewHolder.option_checkbox.setTag(tag);
        //            viewHolder.option_checkbox.setOnClickListener(new View.OnClickListener() {
        //                @Override
        //                public void onClick(View arg0) {
        //                    final CheckBox cb = (CheckBox) arg0;
        //                    final boolean isChecked = cb.isChecked();
        //                    Log.d("isChecked",Boolean.toString(isChecked));
        //                    final Pair<Long, Long> savedTag = (Pair<Long, Long>) arg0.getTag();
        //                    Product p = getGroup(groupPosition);
        //                    p.setSelectedOption(product_option);
        //                    if(cb.isChecked()) {
        //                        mCheckedItems.add(savedTag);
        //                        checkedProducts.add(p);
        //                    } else {
        //                        mCheckedItems.remove(savedTag);
        //                        checkedProducts.remove(p);
        //                    }
        //                }
        //            });


        child = (RelativeLayout) convertView;
        if(isChildChecked(tag)) {
            Log.d("childChecked", Long.valueOf(product_option.getParent_id()) + "," + Long.valueOf(product_option.getId()));
            child.setBackgroundResource(R.color.colorPrimary);
            viewHolder.option_name.setTextColor(Color.WHITE);
            viewHolder.option_price.setTextColor(Color.WHITE);
        } else {
            child.setBackgroundColor(Color.WHITE);
            viewHolder.option_name.setTextColor(primaryColor);
            viewHolder.option_price.setTextColor(primaryColor);
        }

        viewHolder.option_name.setText(product_option.getName());
        viewHolder.option_price.setText(product_option.getPrice());
//        viewHolder.option_checkbox.setChecked(mCheckedItems.contains(tag));

        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return this.products.get(groupPosition).getOptionList().size();
    }

    public Product getGroup(int groupPosition) {
        return this.products.get(groupPosition);
    }

    public int getGroupCount() {
        return this.products.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }


    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String productName = getGroup(groupPosition).getName();
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.group_item,
                    null);
        }
        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                Log.d("onGroupClick","onGroupClick");
                return false;
            }
        });
        //mExpandableListView.expandGroup(groupPosition);

        TextView item = (TextView) convertView.findViewById(R.id.laptop);
        item.setTypeface(null, Typeface.BOLD);
        item.setText(productName);
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
