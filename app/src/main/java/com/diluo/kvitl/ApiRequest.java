package com.diluo.kvitl;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.TextUtils;
//import android.util.Base64;
import org.apache.commons.codec.binary.Base64;
import android.util.Log;
import android.webkit.CookieManager;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.codec.digest.DigestUtils;

import org.apache.commons.codec.digest.HmacUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpCookie;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Stream;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import static java.util.stream.Collectors.joining;

/**
 * Created by beckerz on 10/4/17.
 */

public class ApiRequest {

    String HOSTNAME;
    String ALGORITHM;
    String KEY;
    String SECRET;
    private Context context;

    //public ApiRequest(String api_host, String algorithm, String key, String secret){
    public ApiRequest(SharedPreferences prefs){
        //this.HOSTNAME = api_host;
        //this.ALGORITHM = algorithm;
        //this.KEY = key;
        //this.SECRET = secret;
        this.HOSTNAME = prefs.getString("api_host", "missing");
        this.ALGORITHM = prefs.getString("api_algorithm", "missing");
        this.KEY = prefs.getString("api_key", "missing");
        this.SECRET = prefs.getString("api_secret", "missing");
    }


    static final String AUTH = "/api/auth";
    static final String ALL_CUSTOMERS_QUERY = "/api/customers";
    static final String CUSTOMER_BY_ID = "/api/customer/";
    static final String CUSTOMER_BY_NAME_QUERY = "/api/customers/q/";
    static final String ADD_CUSTOMER = "/api/customer/create";
    static final String ALL_PRODUCTS_QUERY = "/api/products";
    static final String ALL_PRODUCTS_OPTIONS_QUERY = "/api/products_with_options";
    static final String CREDIT_ACCOUNT = "/transaction/credit";
    static final String ACCOUNT_BALANCE = "/api/customer/balance/";
    static final String ADD_TRANSACTION = "/api/transaction/create";
    static final String MARK_TRANSACTION_DELETED = "/api/transaction/delete";
    static final String GET_TRANSACTIONS_BY_ID = "/api/transactions/";
    static final String MARK_CUSTOMER_DELETED = "/api/customer/delete/";
    static final String EDIT_CUSTOMER = "/api/customer/edit";


    static java.net.CookieManager msCookieManager = new java.net.CookieManager();

    public String generate_digest(String secret, String method, String path, String query, String body) {
        try {

            //Log.d("secret", secret);
            //Log.d("method", method);
            //Log.d("path", path);

            if(query.isEmpty()){
                query = "";
            } else {
                //Log.d("query", query);
                query = query.replace("+", "%20");
            }
            if(body.isEmpty()){
                body = "";
            }

            //Log.d("query", query);
            //Log.d("body", body);

            Mac sha256_mac = Mac.getInstance("HmacSHA256");
            sha256_mac.init(new SecretKeySpec(secret.getBytes(), "HmacSHA256"));

            String[] strArr = new String[] {method, path, query};
            String string_data = "";
            for (int i = 0; i < strArr.length; i++){
                String item = strArr[i];
                string_data += item + "n";
            }
            //if(!query.isEmpty()){
                //string_data += query + "n";
            //}
            //Log.d("string_to_hash", Integer.toString(string_data.length()));
            //Log.d("string_to_hash", string_data);

            byte[] hashbytes = sha256_mac.doFinal(string_data.getBytes());
            String new_hash = new String(Hex.encodeHex(hashbytes));

            //Log.d("new_hash", new_hash);

            return new_hash;


        } catch (Exception e) {
            //Log.d("new_hash", e.toString());

            return "";
        }
    }

    public String getAuthToken() {
        String result = null;
        try {
            String queryString = HOSTNAME + AUTH;
            Log.d("queryString", queryString);
            URL url = new URL(queryString);
            String tokenResult = httpRequest(url, "GET");
            JSONObject tokenJson = new JSONObject(tokenResult);
            result = tokenJson.getString("token");
        } catch(Exception e) {
            Log.d("getCustomers", e.toString());
        }
        return result;
    }

    /**
     * Request all customer records
     * @return String
     */
    public String getCustomers(){
        String result = null;
        try {
            String queryString = HOSTNAME + ALL_CUSTOMERS_QUERY;
            Log.d("queryString", queryString);
            URL url = new URL(queryString);
            result = httpRequest(url, "GET", true);
        } catch(Exception e) {
            Log.d("getCustomers", e.toString());
        }
        return result;
    }

    public String getProducts() {
        String result = null;
        try {
            String queryString = HOSTNAME + ALL_PRODUCTS_QUERY;
            URL url = new URL(queryString);
            result = httpRequest(url, "GET", true);
        } catch (Exception e) {
            Log.d("getProducts", e.toString());
        }
        return result;
    }

    public String getProductsWithOptions() {
        String result = null;
        try {
            String queryString = HOSTNAME + ALL_PRODUCTS_OPTIONS_QUERY;
            URL url = new URL(queryString);
            result = httpRequest(url, "GET", true);
        } catch (Exception e) {
            Log.d("getProductsWithOptions", e.toString());
        }
        return result;
    }

    public String getCustomerById(int customer_id) {
        String result = null;
        try {
            String queryString = HOSTNAME + CUSTOMER_BY_ID + customer_id;
            Log.d("queryString", queryString);
            URL url = new URL(queryString);
            result = httpRequest(url, "GET", true);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getCustomerByNameQuery(String queryStr) {
        String result = null;
        try {
            String queryString = HOSTNAME + CUSTOMER_BY_NAME_QUERY + URLEncoder.encode(queryStr, "UTF-8");
            Log.d("queryString", queryString);
            URL url = new URL(queryString);
            result = httpRequest(url, "GET", true);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String addCustomer(String name, String email, String newsletter_opt) {
        String result = null;
        try {
            URL purl = new URL(HOSTNAME + ADD_CUSTOMER);
            result = httpRequest(purl, "GET", true);
//            String cookies = CookieManager.getInstance().getCookie(HOSTNAME + ADD_CUSTOMER);

            String urlParameters= "?customer_name=" + URLEncoder.encode(name, "UTF-8")
                    + "&customer_email=" + URLEncoder.encode(email, "UTF-8")
                    + "&newsletter_opt=" + URLEncoder.encode(newsletter_opt, "UTF-8");

            String queryString = HOSTNAME + ADD_CUSTOMER + urlParameters;
            URL url = new URL(queryString);
            result = httpRequest(url, "POST", true);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String creditAccount(String customer_id, String amount) {
        String result = null;
        try {
            URL purl = new URL(HOSTNAME + CREDIT_ACCOUNT);
            result = httpRequest(purl, "GET", true);

            String urlParameters= "?customer_id=" + URLEncoder.encode(customer_id, "UTF-8") + "&amount=" + URLEncoder.encode(amount, "UTF-8");
            String queryString = HOSTNAME + CREDIT_ACCOUNT + urlParameters;
            URL url = new URL(queryString);
            result = httpRequest(url, "POST", true);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getAccountBalance(int customer_id) {
        String result = null;
        try {
            String queryString = HOSTNAME + ACCOUNT_BALANCE + URLEncoder.encode(Integer.toString(customer_id), "UTF-8");
            Log.d("queryString", queryString);
            URL url = new URL(queryString);
            result = httpRequest(url, "GET", true);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String addTransaction(String cust_id, String total, String[] product_option_ids) {
        String result = null;
        try {
            // acquiring xsrf
            URL purl = new URL(HOSTNAME + ADD_TRANSACTION);
            result = httpRequest(purl, "GET", true);

            // customer_id
            // total
            // items

            String urlParameters= "?customer_id=" + URLEncoder.encode(cust_id, "UTF-8") +
                    "&total=" + URLEncoder.encode(total, "UTF-8");
            for(String p:product_option_ids) {
                urlParameters += "&items=" + p;
            }
            String queryString = HOSTNAME + ADD_TRANSACTION + urlParameters;
            URL url = new URL(queryString);
            result = httpRequest(url, "POST", true);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String markTransactionDeleted(String transaction_id) {
        String result = null;
        try {
            // acquiring xsrf
            URL purl = new URL(HOSTNAME + MARK_TRANSACTION_DELETED);
            result = httpRequest(purl, "GET", true);

            String urlParameters= "?transaction_id=" + URLEncoder.encode(transaction_id, "UTF-8");

            String queryString = HOSTNAME + MARK_TRANSACTION_DELETED + urlParameters;
            URL url = new URL(queryString);
            result = httpRequest(url, "POST", true);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getTransactionsById(int customer_id) {
        String result = null;
        try {
            //URL purl = new URL(HOSTNAME + GET_TRANSACTIONS_BY_ID);
            // result = httpRequest(purl, "GET");

            String queryString = HOSTNAME + GET_TRANSACTIONS_BY_ID + customer_id;
            URL url = new URL(queryString);
            result = httpRequest(url, "GET", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String markCustomerDeleted(String customer_id) {
        String result = null;
        try {
            String queryString = HOSTNAME + MARK_CUSTOMER_DELETED + customer_id;
            URL url = new URL(queryString);
            result = httpRequest(url, "GET", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String EditCustomer(String customer_id, String cust_name, String newsletter_opt) {
        String result = null;
        try {
            // acquiring xsrf
            URL purl = new URL(HOSTNAME + EDIT_CUSTOMER);
            result = httpRequest(purl, "GET", true);

            String urlParameters= "?customer_id=" + URLEncoder.encode(customer_id, "UTF-8") +
                "&customer_name=" + URLEncoder.encode(cust_name, "UTF-8") +
                "&newsletter_opt=" + URLEncoder.encode(newsletter_opt, "UTF-8");

                String queryString = HOSTNAME + EDIT_CUSTOMER + urlParameters;
            Log.d("urlString", queryString);
            URL url = new URL(queryString);
            result = httpRequest(url, "POST", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * Given a URL, sets up a connection and gets the HTTP response body from the server.
     * If the network request is successful, it returns the response body in String form. Otherwise,
     * it will throw an IOException.
     */
    private String httpRequest(URL url, String method) throws IOException {
        return httpRequest(url, method, false);
    }

    private String httpRequest(URL url, String method, boolean digest) throws IOException {
        InputStream stream = null;
        HttpsURLConnection connection = null;
        String result = null;
        try {
            connection = (HttpsURLConnection) url.openConnection();
            // Timeout for reading InputStream arbitrarily set to 3000ms.
            connection.setReadTimeout(3000);
            // Timeout for connection.connect() arbitrarily set to 3000ms.
            connection.setConnectTimeout(3000);
            // For this use case, set HTTP method to GET.
            connection.setRequestMethod(method);
            // Already true by default but setting just in case; needs to be true since this request
            // is carrying an input (response) body.
            connection.setDoInput(true);

            // Cookies
            if (msCookieManager.getCookieStore().getCookies().size() > 0) {
                Log.d("cookie", "cookies being set");
                Log.d("cookie", TextUtils.join(";",  msCookieManager.getCookieStore().getCookies()));
                // While joining the Cookies, use ',' or ';' as needed. Most of the servers are using ';'
                String cookie = TextUtils.join(";",  msCookieManager.getCookieStore().getCookies());
                //String val = token.replace("_xsrf=","");
                Log.d("val", cookie);
                connection.setRequestProperty("Cookie",cookie);
                connection.setRequestProperty("X-CSRFToken",cookie.replace("_xsrf=",""));
            }
            if(digest == true) {
                //String authStr = "Bearer " + token;
                String query = "";
//                String algorithm = "KVITL-V1";
//                String key = "my_key";
//                String secret = "my_secret_key";
                String algorithm = this.ALGORITHM;
                String key = this.KEY;
                String secret = this.SECRET;

                String path = url.getPath();
                try {
                    //query = url.toString();
                    String[] q = url.toString().split("\\?");
                    String[] params = q[1].split("&");
                    Log.d("query_string", url.getQuery());
                    //query = q[1];
                    Arrays.sort(params);
                    query = TextUtils.join("&", params);
                } catch(Exception e) {
                    Log.d("no_query", "no_query");
                }
                String body = "";
                // String secret, String method, String path, String query, String body
                String generated_digest = generate_digest(secret, method, path, query, body);
                String authStr = TextUtils.join(" ", new String[] {algorithm, key, generated_digest});
                connection.setRequestProperty ("Authorization", authStr);
            }

            // Open communications link (network traffic occurs here).
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode != HttpsURLConnection.HTTP_OK) {
                throw new IOException("HTTP error code: " + responseCode);
            }
            Map<String, List<String>> map = connection.getHeaderFields();
            for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                System.out.println("Key : " + entry.getKey() +
                        " ,Value : " + entry.getValue());
            }
            final String COOKIES_HEADER = "Set-Cookie";
            List<String> cookiesHeader = map.get(COOKIES_HEADER);
            if (cookiesHeader != null) {
                for (String cookie : cookiesHeader) {
                    Log.d("cookie", cookie);
                    Log.d("cookiesHeader", HttpCookie.parse(cookie).get(0).toString());
                    msCookieManager.getCookieStore().add(null,HttpCookie.parse(cookie).get(0));
                }
            }


            // Retrieve the response body as an InputStream.
            stream = connection.getInputStream();
            if (stream != null) {
                // Converts Stream to String with max length of 500.
                //result = readStream(stream, 1500);
                result = bufferStream(stream);

            }
        } finally {
            // Close Stream and disconnect HTTPS connection.
            if (stream != null) {
                stream.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }

    private String bufferStream(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(stream, "UTF-8"));

        // Grab the results
        StringBuilder log = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            log.append(line + "\n");
        }
        return log.toString();
    }


    /**
     * Converts the contents of an InputStream to a String.
     */
    private String readStream(InputStream stream, int maxLength) throws IOException {
        String result = null;
        // Read InputStream using the UTF-8 charset.
        InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
        // Create temporary buffer to hold Stream data with specified max length.
        char[] buffer = new char[maxLength];
        // Populate temporary buffer with Stream data.
        int numChars = 0;
        int readSize = 0;
        while (numChars < maxLength && readSize != -1) {
            numChars += readSize;
            int pct = (100 * numChars) / maxLength;
            readSize = reader.read(buffer, numChars, buffer.length - numChars);
        }
        if (numChars != -1) {
            // The stream was not empty.
            // Create String that is actual length of response body if actual length was less than
            // max length.
            numChars = Math.min(numChars, maxLength);
            result = new String(buffer, 0, numChars);
        }
        return result;
    }
}

