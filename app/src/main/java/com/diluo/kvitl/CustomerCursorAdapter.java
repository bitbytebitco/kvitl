package com.diluo.kvitl;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by Bit Byte Bit on 10/1/17.
 */

public class CustomerCursorAdapter extends CursorAdapter {
    public CustomerCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.row_layout, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView listText = (TextView) view.findViewById(R.id.customerName);

        // Extract properties from cursor
        String fn = cursor.getString(cursor.getColumnIndexOrThrow(CustomerReaderContract.CustomerEntry.COLUMN_FIRST_NAME));
        String ln = cursor.getString(cursor.getColumnIndexOrThrow(CustomerReaderContract.CustomerEntry.COLUMN_LAST_NAME));
        String full_name = fn + " " + ln;

        String cust_enabled = cursor.getString(cursor.getColumnIndexOrThrow(CustomerReaderContract.CustomerEntry.COLUMN_ENABLED));
        // Populate fields with extracted properties

        Log.d("cust_enabled1", " ");
        Log.d("cust_enabled2", cust_enabled);
        Log.d("cust_enabled3", full_name);

        listText.setText(full_name);
        if(cust_enabled.equals("1")) {
            listText.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else if(cust_enabled.equals("0")) {
            listText.setTextColor(ContextCompat.getColor(context, R.color.disabled_grey));
        }
    }

    @Override
    public boolean isEnabled(int position) {
        /*
        Cursor cursor = this.getCursor();
        String enabled = cursor.getString(cursor.getColumnIndexOrThrow(CustomerReaderContract.CustomerEntry.COLUMN_ENABLED));
        switch(enabled){
            case "0":
                return false;
            case "1":
                return true;
        }
        */
        return true;
    }
}
