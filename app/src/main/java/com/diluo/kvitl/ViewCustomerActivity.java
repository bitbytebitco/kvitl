package com.diluo.kvitl;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by beckerz on 10/4/17.
 */

public class ViewCustomerActivity extends AppCompatActivity implements AddCreditDialogFragment.DialogListener, DeleteTransactionDialogFragment.DialogListener{

    static final int ADD_TICKET_REQUEST = 111;
    static final int EDIT_CUSTOMER_REQUEST = 112;

    public void networkErrorDialog(){
        NetworkErrorDialogFragment newFragment = new NetworkErrorDialogFragment();
        newFragment.show(this.getSupportFragmentManager(), "network_error");
    }

    private class GetCustomerTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            int customer_id = Integer.parseInt(params[0]);
            String customer = apiRequest.getCustomerById(customer_id);
            return customer;
        }

        @Override
        protected void onPreExecute() {
            Connection conn = new Connection();
            if(!conn.isConnected(getApplicationContext())){
                networkErrorDialog();
                cancel(true);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                JSONObject custJson = new JSONObject(result);
                Customer customer = Customer.fromJson(custJson);

                TextView customerName = findViewById(R.id.customerName);
                customerName.setText(customer.getName());

                TextView tabAmount = findViewById(R.id.tabAmount);
                tabAmount.setText(customer.getTotal());

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void triggerTransactionListRefresh() {
        final String customer_id = getIntent().getStringExtra("CUSTOMER_ID");
        GetTransactionsTask getTransactionsTask = new GetTransactionsTask();
        getTransactionsTask.execute(customer_id);
    }

    public void updateTransactionList(ArrayList<Transaction> transactions) {
        TransactionsListAdapter transactionsListAdapter = new TransactionsListAdapter(getApplicationContext(), transactions );
        ListView transaction_list = (ListView) findViewById(R.id.tab_transactions_list);
        transaction_list.setAdapter(transactionsListAdapter);

        transaction_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
            Log.d("trans_view_list_id", Integer.toString(i));
            Transaction transaction = (Transaction) adapterView.getItemAtPosition(i);

            final String customer_id = getIntent().getStringExtra("CUSTOMER_ID");
            deleteTransactionDialog(transaction.getTransaction_id(), customer_id);
            return false;
            }
        });
    }

    public void triggerGetCustomerInfo(String customer_id){
        GetCustomerTask customerTask = new GetCustomerTask();
        customerTask.execute(customer_id);
    }

    public class GetTransactionsTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            int customer_id = Integer.parseInt(params[0]);
            String customer = apiRequest.getTransactionsById(customer_id);
            return customer;
        }

        @Override
        protected void onPreExecute() {
            Connection conn = new Connection();
            if(!conn.isConnected(getApplicationContext())){
                networkErrorDialog();
                cancel(true);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                // get json result
                JSONArray transactionsJson = new JSONArray(result);
                ArrayList<Transaction> transactions = Transaction.fromJson(transactionsJson);

                // list view adapter
                updateTransactionList(transactions);

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class GetBalanceTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            int customer_id = Integer.parseInt(params[0]);
            try {
                String customer = apiRequest.getAccountBalance(customer_id);
                return customer;
            } catch(Exception e){
                Log.d("params", params[0]);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Connection conn = new Connection();
            if(!conn.isConnected(getApplicationContext())){
                networkErrorDialog();
                cancel(true);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                JSONObject balJson = new JSONObject(result);
                String balance = balJson.getString("balance");
                Log.d("balance", balance);

                TextView tabAmount = findViewById(R.id.tabAmount);

                BigDecimal balance_amt = new BigDecimal(balance);
                if(balance_amt.compareTo(BigDecimal.ZERO) > 0){
                    tabAmount.setTextAppearance(R.style.CodeFont);
                } else {
                    tabAmount.setTextAppearance(R.style.NegativeBalance);
                }
                tabAmount.setText(balance_amt.toString());

            } catch(Exception e) {
                Log.d("problem", e.toString());
                e.printStackTrace();
            }
        }
    }

    public void refreshBalance() {
        final String customer_id = getIntent().getStringExtra("CUSTOMER_ID");
        GetBalanceTask balanceTask = new GetBalanceTask();
        balanceTask.execute(customer_id);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final String customer_id = getIntent().getStringExtra("CUSTOMER_ID");
        triggerTransactionListRefresh();
        triggerGetCustomerInfo(customer_id);
        refreshBalance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_customer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.ic_rostovs_r);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final String customer_id = getIntent().getStringExtra("CUSTOMER_ID");

        // get customer information
        triggerGetCustomerInfo(customer_id);

        // get transactions
        GetTransactionsTask getTransactionsTask = new GetTransactionsTask();
        getTransactionsTask.execute(customer_id);

        // update balance view
        refreshBalance();

        // Add User Button
        FloatingActionButton fab_add_user = (FloatingActionButton) findViewById(R.id.fab_add_transaction);
        fab_add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("new_transaction", "new_transaction");

                Intent intent = new Intent(getApplicationContext(), AddTicketActivity.class);
                intent.putExtra("CUSTOMER_ID", customer_id);
                startActivityForResult(intent, ADD_TICKET_REQUEST);
            }
        });

        FloatingActionButton fab_add_credit = (FloatingActionButton) findViewById(R.id.fab_add_credit);
        fab_add_credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("add_credit", "add_credit");
                addCreditDialog(customer_id);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        Log.d("onActivityResult", Integer.toString(requestCode));
        if (requestCode == ADD_TICKET_REQUEST || requestCode == EDIT_CUSTOMER_REQUEST) {
            Log.d("requestCode", Integer.toString(requestCode));
            Log.d("resultCode", Integer.toString(resultCode));
            Log.d("RESULT_OK", Integer.toString(RESULT_OK));
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                refreshBalance();
            }
        }
    }

    public void addCreditDialog(String customer_id) {
        DialogFragment addCreditFragment = AddCreditDialogFragment.newInstance(customer_id);
        addCreditFragment.show(this.getFragmentManager(), "add_credit");
    }

    public void deleteTransactionDialog(String transaction_id, String customer_id) {
        DeleteTransactionDialogFragment deleteTransactionDialogFragmentFragment = DeleteTransactionDialogFragment.newInstance(transaction_id, customer_id);
        deleteTransactionDialogFragmentFragment.show(this.getFragmentManager(), "delete_transaction_dialog");
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("menu item: ", Integer.toString(item.getItemId()));
        switch(item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_edit_customer:
                final String customer_id = getIntent().getStringExtra("CUSTOMER_ID");
                Intent intent = new Intent(getApplicationContext(), EditCustomerActivity.class);
                intent.putExtra("CUSTOMER_ID", customer_id);
                startActivityForResult(intent, EDIT_CUSTOMER_REQUEST);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

//        Cursor record = getCustomerEntry();
//        String is_enabled = record.getString(record.getColumnIndexOrThrow(CustomerReaderContract.CustomerEntry.COLUMN_ENABLED));
//        try {
//            if(is_enabled.equals("0")) {
//                inflater.inflate(R.menu.view_customer_menu_disabled, menu);
//            } else if (is_enabled.equals("1")) {
//                inflater.inflate(R.menu.view_customer_menu_enabled, menu);
//            }
//        } finally {
//            record.close();
//        }
        inflater.inflate(R.menu.view_customer_menu_disabled, menu);
        return true;
    }
}
