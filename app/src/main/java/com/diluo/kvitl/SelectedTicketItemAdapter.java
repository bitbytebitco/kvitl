package com.diluo.kvitl;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by beckerz on 10/20/17.
 */

public class SelectedTicketItemAdapter extends ArrayAdapter<Product> {

    ArrayList<Product> products;

    private class ViewHolder {
        TextView productName;
        TextView productOptionPrice;
        Button removeItemButton;
    }

    public SelectedTicketItemAdapter(Context context, ArrayList<Product> products) {
        super(context, 0, products);
        this.products = products;
    }

    public String getTotal() {
        BigDecimal total = new BigDecimal(0);
        for(Product p: this.products) {
            total = total.add(new BigDecimal(p.getSelected_option().getPrice()));
            Log.d("price", p.getSelected_option().getPrice());
        }
        Log.d("total", total.toString());
        return total.toString();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Product product = getItem(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.selected_item_row, parent, false);
            viewHolder.productName = (TextView) convertView.findViewById(R.id.selectedProductName);
            viewHolder.productOptionPrice = (TextView) convertView.findViewById(R.id.productOptionPrice);
            viewHolder.removeItemButton = (Button) convertView.findViewById(R.id.removeItemButton);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.productName.setText(product.getName() + " - " + product.getSelected_option().getName());
        viewHolder.productOptionPrice.setText(product.getSelected_option().getPrice());
        viewHolder.removeItemButton.setTag(position);
        viewHolder.removeItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int)view.getTag();
                products.remove(pos);
                SelectedTicketItemAdapter.this.notifyDataSetChanged();
            }
        });
        return convertView;
    }

}
