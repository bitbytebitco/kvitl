package com.diluo.kvitl;

import android.provider.BaseColumns;

/**
 * Created by softchalk on 2/18/17.
 */

public final class TicketReaderContract {
    private TicketReaderContract() {}

    public static class Ticket implements BaseColumns {
        public static final String TABLE_NAME = "tab_notes";
        public static final String COLUMN_NAME_CUSTOMER_ID = "customer_id";
        public static final String COLUMN_NAME_NOTE_TYPE = "type";
        public static final String COLUMN_NAME_NOTE_AMOUNT = "amount";
    }

}
