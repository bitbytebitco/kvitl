package com.diluo.kvitl;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import org.json.JSONObject;

/**
 * Created by beckerz on 11/15/17.
 */

public class ViewEditCustFragment extends Fragment {
    static ViewEditCustFragment newInstance(String customer_id){
        ViewEditCustFragment f = new ViewEditCustFragment();
        Bundle args = new Bundle();
        args.putString("customer_id", customer_id);
        f.setArguments(args);
        return f;
    }

    public void networkErrorDialog(){
        NetworkErrorDialogFragment newFragment = new NetworkErrorDialogFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "network_error");
    }

    private class GetCustomerTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            int customer_id = Integer.parseInt(params[0]);
            String customer = apiRequest.getCustomerById(customer_id);
            return customer;
        }

        @Override
        protected void onPreExecute() {
            Connection conn = new Connection();
            if(!conn.isConnected(getContext())){
                networkErrorDialog();
                cancel(true);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("onPostExecute", result);
            try {
                JSONObject custJson = new JSONObject(result);
                Customer customer = Customer.fromJson(custJson);

                TextView customerName = (TextView) getActivity().findViewById(R.id.cust_name);
                customerName.setText(customer.getName());

                TextView customerEmail = (TextView) getActivity().findViewById(R.id.cust_email);
                customerEmail.setText(customer.getEmail());

//                CheckBox coffeeClubCheckbox = getActivity().findViewById(R.id.coffee_chkbox);
//                if(customer.getCoffeeClubFlag()){
//                    coffeeClubCheckbox.setChecked(true);
//                } else {
//                    coffeeClubCheckbox.setChecked(false);
//                }

                CheckBox newsletterClubCheckbox = getActivity().findViewById(R.id.newsletter_chkbox);
                if(customer.getNewsletterFlag()){
                    newsletterClubCheckbox.setChecked(true);
                } else {
                    newsletterClubCheckbox.setChecked(false);
                }

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.view_edit_customer_view, container, false);

        Bundle bundle = getArguments();
        final String customer_id = bundle.getString("customer_id");
        GetCustomerTask customerTask = new GetCustomerTask();
        customerTask.execute(customer_id);

        return layout;
    }
}
