package com.diluo.kvitl;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import org.json.JSONObject;

/**
 * Created by beckerz on 2/8/18.
 */

public class DeleteTransactionDialogFragment extends DialogFragment {
    static DeleteTransactionDialogFragment newInstance(String transaction_id, String customer_id){
        DeleteTransactionDialogFragment f = new DeleteTransactionDialogFragment();
        Bundle args = new Bundle();

        Log.d("transaction_id_2", transaction_id);
        Log.d("customer_id_2", customer_id);

        args.putString("transaction_id", transaction_id);
        args.putString("customer_id", customer_id);
        f.setArguments(args);
        return f;
    }

    public interface DialogListener {
        void triggerTransactionListRefresh();
        void refreshBalance();
    }

    DialogListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DialogListener) context;
        } catch (ClassCastException e) {
            Log.d("class_error", e.getMessage());
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    private class MarkTransactionDeletedTask extends AsyncTask<Object, Integer, String> {
        @Override
        protected String doInBackground(Object... params){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            ApiRequest apiRequest = new ApiRequest(prefs);
            String customer_id = (String) params[0];

            String deleteTransactionResp = apiRequest.markTransactionDeleted(customer_id);
            return deleteTransactionResp;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("onPostExecute", result);
                JSONObject resultJson = new JSONObject(result);
                if(resultJson.getString("result").equals("ok")) {
                    Log.d("delete_worked", "delete_worked");

                    Bundle bundle = getArguments();
                    final String customer_id = bundle.getString("customer_id");
                    Log.d("customer_id_attempt", customer_id);

                    mListener.refreshBalance();
                    mListener.triggerTransactionListRefresh();
                }

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        Bundle bundle = getArguments();
        final String transaction_id = bundle.getString("transaction_id");

        builder.setMessage(R.string.delete_transaction_message)
                .setPositiveButton(R.string.confirm_delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("cust_id_2", transaction_id);

                        MarkTransactionDeletedTask deleteTransactionTask = new MarkTransactionDeletedTask();
                        deleteTransactionTask.execute(transaction_id);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dismiss();
                    }
                });

        return builder.create();
    }
}
