package com.diluo.kvitl;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class ViewCustomerActivityOld extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_customer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Cursor record = getCustomerEntry();
        try {
            TextView customerName = (TextView) findViewById(R.id.customerName);
            String fn = record.getString(record.getColumnIndexOrThrow(CustomerReaderContract.CustomerEntry.COLUMN_FIRST_NAME));
            String ln = record.getString(record.getColumnIndexOrThrow(CustomerReaderContract.CustomerEntry.COLUMN_LAST_NAME));
            String cn = fn + " " + ln;
            Log.d("VIEW", cn);
            customerName.setText(cn);

            TextView customerEmail = (TextView) findViewById(R.id.customerEmail);
            String email = record.getString(record.getColumnIndexOrThrow(CustomerReaderContract.CustomerEntry.COLUMN_EMAIL));
            Log.d("VIEW", email);
            customerEmail.setText(email);
        } finally {
            record.close();
        }
    }

    public Cursor getCustomerEntry(){
        String record_id = getIntent().getStringExtra("RECORD_ID");
        KvitlDbHelper mDbHelper = new KvitlDbHelper(ViewCustomerActivityOld.this);

        Log.d("VIEW", "View Customer");
        Log.d("VIEW", record_id);

        Cursor record = mDbHelper.getRecordsWhereId(Integer.parseInt(record_id));

        try {
            if (record.moveToFirst()) {
                return record;
            }
        } catch (Exception e) {
            Log.d("db_problem", "Prollems");
            Log.d("db_problem", e.getMessage());
        }
        return null;
    }

    public void deleteCustomerDialog(){
        DialogFragment newFragment = new DeleteCustomerDialogFragment();
        newFragment.show(this.getSupportFragmentManager(), "delete_customer");
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("menu item: ", Integer.toString(item.getItemId()));
        switch(item.getItemId()){
            case R.id.action_add_ticket:
                Intent myIntent = new Intent(getApplicationContext(), AddTicketActivity.class);
                startActivityForResult(myIntent, 0);
                return true;
            case R.id.action_edit_customer:
                deleteCustomerDialog(); // Place this in EditCustomerActivity
                return true;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        Cursor record = getCustomerEntry();
        String is_enabled = record.getString(record.getColumnIndexOrThrow(CustomerReaderContract.CustomerEntry.COLUMN_ENABLED));
        try {
            if(is_enabled.equals("0")) {
                inflater.inflate(R.menu.view_customer_menu_disabled, menu);
            } else if (is_enabled.equals("1")) {
                inflater.inflate(R.menu.view_customer_menu_enabled, menu);
            }
        } finally {
            record.close();
        }
        return true;
    }

}
