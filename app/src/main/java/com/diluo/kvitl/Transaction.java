package com.diluo.kvitl;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by beckerz on 11/15/17.
 */

public class Transaction {
    public String transaction_id;
    public String customer_id;
    public String trans_type;
    public String trans_date;
    public String trans_total;

    public Transaction(String transaction_id, String customer_id, String trans_type, String trans_date, String trans_total) {
        this.transaction_id = transaction_id;
        this.customer_id = customer_id;
        this.trans_type = trans_type;
        this.trans_date = trans_date;
        this.trans_total = trans_total;
    }

    public String getTransaction_id () { return this.transaction_id; }

    public String getCustomer_id () { return this.customer_id; }

    public String getTrans_type () { return this.trans_type; }

    public String getTrans_date () {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat formatter = new SimpleDateFormat("E, MMM dd, yyyy");
            Log.d("date", this.trans_date);
            Date date = sdf.parse(this.trans_date);
            return formatter.format(date);
        } catch(Exception e) {
            Log.d("date_issue", "date_issue");
            Log.d("date", e.toString());
            return this.trans_date;
        }
    }

    public String getTrans_total () { return this.trans_total; }

    public static Transaction fromJson(JSONObject jsonTransaction) {
        Transaction t = null;

        try {
            String transaction_id = jsonTransaction.getString("id");
            String customer_id = jsonTransaction.getString("customer_id");
            String trans_type = jsonTransaction.getString("trans_type");
            String trans_date = jsonTransaction.getString("trans_date");
            String trans_total = jsonTransaction.getString("trans_total");

            t = new Transaction(transaction_id, customer_id, trans_type, trans_date, trans_total);

        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
        return t;
    }

    public static ArrayList fromJson(JSONArray jsonArray) {
        JSONObject transactionJson;
        ArrayList<Transaction> transactions = new ArrayList<Transaction>(jsonArray.length());
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    transactionJson = jsonArray.getJSONObject(i);
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
                Transaction transaction = Transaction.fromJson(transactionJson);
                if (transaction != null) {
                    transactions.add(transaction);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return transactions;
    }

}
